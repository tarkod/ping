package fr.tarkod.pingplus;

import org.bukkit.plugin.java.JavaPlugin;

import fr.tarkod.pingplus.commands.Ip;
import fr.tarkod.pingplus.commands.Ping;
import fr.tarkod.pingplus.commands.Reload;

public class PingPlusMain extends JavaPlugin {

	private static PingPlusMain instance;

	public static PingPlusMain getInstance() {
		return instance;
	}

	@Override
	public void onEnable() {
		instance = this;
		saveDefaultConfig();

		getCommand("ping").setExecutor(new Ping());
		getCommand("cprl").setExecutor(new Reload());
		getCommand("ip").setExecutor(new Ip());
	}
}