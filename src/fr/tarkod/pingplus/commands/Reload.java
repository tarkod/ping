package fr.tarkod.pingplus.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tarkod.pingplus.PingPlusMain;
import fr.tarkod.pingplus.enumeration.ConfigString;
import fr.tarkod.pingplus.utils.ConfigManager;

public class Reload implements CommandExecutor {
	
	static PingPlusMain main = PingPlusMain.getInstance();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("cprl")) {
				String fileName = "config";
				ConfigManager.loadConfig(fileName);
				ConfigManager.saveConfig(fileName);
				p.sendMessage(ConfigString.RELOAD.getString());
			}
		}
		return false;
	}
}
