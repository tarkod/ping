package fr.tarkod.pingplus.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.tarkod.pingplus.enumeration.ConfigString;

public class Ip implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("ip")) {
				if(args.length == 0) {
					p.sendMessage(ConfigString.YOUR_IP.getString(p));
				}
				if(args.length >= 1) {
					if(Bukkit.getPlayer(args[0]) != null) {
						Player o = Bukkit.getPlayer(args[0]);
						p.sendMessage(ConfigString.THEIR_IP.getString(o));
					} else {
						p.sendMessage(ConfigString.OFFLINE_PLAYER.getString(args[0]));
					}
				}
			}
		}
		return false;
	}
}
