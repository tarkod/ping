package fr.tarkod.pingplus.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import fr.tarkod.pingplus.PingPlusMain;

public class ConfigManager {

	private static FileConfiguration config = YamlConfiguration.loadConfiguration(getFile("config"));

	public static FileConfiguration getDefaultConfig() {
		return config;
	}

	public static void createFile(String fileName) {
		if (!PingPlusMain.getInstance().getDataFolder().exists()) {
			PingPlusMain.getInstance().getDataFolder().mkdir();
		}
		File file = new File(PingPlusMain.getInstance().getDataFolder(), fileName + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static File getFile(String fileName) {
		return new File(PingPlusMain.getInstance().getDataFolder(), fileName + ".yml");
	}

	public static void saveConfig(String fileName) {
		try {
			config.save(getFile(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void loadConfig(String fileName) {
		try {
			config.load(getFile(fileName));
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	public static FileConfiguration getConfig() {
		return config;
	}

}
