package fr.tarkod.pingplus.utils;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import fr.tarkod.pingplus.PingPlusMain;

public class PingManager {

	static PingPlusMain main = PingPlusMain.getInstance();
	static FileConfiguration file = main.getConfig();

	public static int getPing(Player p) {
		try {
			Object entityPlayer = p.getClass().getMethod("getHandle").invoke(p);
			return (int) entityPlayer.getClass().getField("ping").get(entityPlayer);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException | NoSuchFieldException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
