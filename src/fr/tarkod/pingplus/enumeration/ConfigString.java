package fr.tarkod.pingplus.enumeration;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import fr.tarkod.pingplus.utils.ConfigManager;
import fr.tarkod.pingplus.utils.PingManager;

public enum ConfigString {
	
	YOUR_IP ("your_ip"),
	THEIR_IP ("their_ip"),
	YOUR_PING ("your_ping"),
	THEIR_PING ("their_ping"),
	OFFLINE_PLAYER ("offline_player"),
	RELOAD ("reload");
	
	//Import
	FileConfiguration file = ConfigManager.getConfig();
	
	//La variable d�fini par le constructeur.
	String name;
   
	//Constructeur
	ConfigString(String name){
		this.name = name;
	}
   
	public String getString(Player p) {
		return file.getString(name)
				.replace("%player%", p.getName())
				.replace("%ip%", p.getAddress().getHostString())
				.replace("%ping%", "" + PingManager.getPing(p))
				.replace("%port%", "" + p.getAddress().getPort());
	}
	
	public String getString() {
		return file.getString(name);
	}
	
	public String getString(String player) {
		return file.getString(name)
				.replace("%player%", player);
	}
}
